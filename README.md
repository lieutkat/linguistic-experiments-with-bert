# About the repository
The repository contains a software implementation of experiments with several variants of the BERT language model in the probing task of masked language modeling.

The presented Russian language corpus consists of three subcorpora, each of them has 50 sentences. The total volume is about 2500 words, or 20,600 characters. Subcorpora are designed to test and evaluate a certain aspect of the functioning of the language model: common sense inference, the interpretation of semantic roles, and processing negatives depending on the context. Russian Corpus was assembled manually with the help of texts for students of Russian as a foreign language, provided by researchers of the Higher School of Economics and the Russian National Corpus (RNC).

# How to use
Copy the content of the repository to your Google Drive and write down the paths to the files with subcorpora. Then start the notebook and see the results.

# Key words
language models, BERT, linguistic of language models, masked language modeling, language models for Russian

# Contacts
Feel free to ask your questions: e.vyazemskaya@yandex.ru

